# Contribution Guide (wip):

## General

All code contributions are expected to be clean code.
This means the code generally adheres to the principles and practices espoused by Uncle Bob (Robert Martin) in his
classes, books, blog posts, articles and videos.
For classes, books, blog posts and articles see [cleancoder.com](http://cleancoder.com/products).
For videos see both [cleancoders.com](https://cleancoders.com) and YouTube where you can find many free talks and
presentations.

Clean code for the projects also means the code adheres to the
[Kotlin Coding Conventions](https://kotlinlang.org/docs/coding-conventions.html) for Kotlin version 1.8.10.

## Pull Requests (PR)
For a PR to be accepted (merged) the following will be evaluated and considered:

Is the source code clean? Is it spotless? Does it have a bad smell?
- Are there spurious and unnecessary comments?
- Are functions of length ~4 lines long, preferably less?
- Is block structure employed (see Structure and Interpretation fo Computer Programs - SICP, by Abelson & Sussman,
MIT Press)
- Does the source code scream the project name?
- Do names reveal intent?
- Was the code developed using TDD?
- Is the code 100% code covered as measured by Kover?
- Are there no warnings?
- Does the code adhere to the SOLID Single Responsibility Principle (SRP)?
- Is the (SRP) single reason for change identified in classes, objects, interfaces and functions?

Does the PR address a bug, issue, etc. identified and possibly discussed on the relevant GitLab project?

## Hardware

I often encounter a would-be contributor who shows up expecting to do Android development with a computer equipped with
4G or less of RAM, 256G or less of hard drive, a very low bandwidth internet connection (<20Mbps), a very small display
(laptop or old, small low-resolution monitor). My expectations from a developer with such equipment are very low.

In 2023, this is what I would consider acceptable levels of hardware in order to do modern Android project development:

- Base Level: 16G RAM, 512G SSD storage, 24" or better 4K monitor, ~100Mbps internet access
- Decent level: 32G RAM, 1TB SSD storage, 27" or better 4K monitor (maybe two), ~500Mbps internet access
- Awesome level: 64G or greater RAM, 2TB or more SSD storage, 27" or greater (or multiple) 5K or better monitor, 1Gbps
or better internet access.
