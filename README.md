# tks-doc

Version 0.9.1

This project exists to provide detailed documentation about the
[TMDB Kotlin SDK (TKS)](https://gitlab.com/pajato/tmdb-kotlin-sdk) projects.

This file exists to get you started on the road to understanding, critiquing and/or contributing to the projects.

## TMDB Background

The Movie Database (TMDB) is/was the free (as in beer) heir apparent to IMDB upon Amazon's purchase of IMDB. This
[FAQ](https://www.themoviedb.org/faq/general) provides an excellent introduction to the IMDB background and features.

### Status

The project has started and does provide a useful set of APIs but one that unlocks only a fraction of the complete TMDB
API catalog for Kotlin developers.

## Licensing

All Pajato applications and libraries are Free Software (source code is freely available) and published using the
[Gnu General Public License, V3](https://www.gnu.org/licenses/gpl-3.0.en.html).

## Pajato Software

Pajato software can be characterized on two levels: Clean Code and Kotlin Multiplatform

### Clean Code

The Pajato projects strive to provide clean code as espoused by Uncle Bob's
[clean coder organization](https://cleancoders.com/).

This means:

- Functions are small, usually 1 to 4 lines long, never more.
- Design and implementation is driven by tests. 100% test coverage is mandatory and enforced at build time.
- Names are meant to reveal intent. Naming is hard so names are frequently changed to better reveal intent.
- Function structure is simple: only a few arguments, adherence to the use of
[CQS](https://en.wikipedia.org/wiki/Command%E2%80%93query_separation#:~:text=Command%2Dquery%20separation%20(CQS),the%20caller%2C%20but%20not%20both.)
, use of [Screaming Architecture](https://blog.cleancoder.com/uncle-bob/2011/09/30/Screaming-Architecture.html), etc.
- The [architecture](https://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html)
consists of four layers: Core (Entity), Interactor (Use Cases), Adapter (Interface Adapters), Main (Frameworks &
Drivers)
- Independent developability by layers (see more below)
- [SOLID principles](https://blog.cleancoder.com/uncle-bob/2020/10/18/Solid-Relevance.html) are standard
- [Functional programming](https://blog.cleancoder.com/uncle-bob/2018/04/13/FPvsOO.html) style is encouraged and used
heavily, but not exclusively.
- The [Booch Principle](https://www.goodreads.com/quotes/7029841-clean-code-is-simple-and-direct-clean-code-reads-like)
is also heavily used: "Clean code is simple and direct. It reads like well-written prose." See more below.

### Kotlin Multiplatform

All TKS source code is written in the Kotlin language and complies with the
[Kotlin Coding Standard guidelines](https://kotlinlang.org/docs/coding-conventions.html).

The [Kotlin Multiplatform](https://kotlinlang.org/docs/multiplatform.html) capability provides the glue that allows
applications and libraries to work across Android, iOS, JavaScript, JVM and native OS code. The gist of Kotlin
Multiplatform is that most code is written using a "common" base and specific target code is written using an escape
mechanism using ```expect``` and ```actual``` keywords. See the
[Jetbrains Kotlin documentation](https://kotlinlang.org/docs/home.html) for all Kotlin details. It is excellent.

### Independent Developability

The reason independent developability is important is to minimize source code control conflict resolution, a form of
software development hell that has existed for decades. It all began the day after two developers started working on a
single code base.

The key to independent developability is to divide an application into many projects where each project is developed by
a very small team. These projects each produce a "deployable" product that is combined into a versioned collection used
to build the single application executable. Think Android APK, Web war file, Windows (or Linux or Mac) native
executable, etc.

This division works well to minimize conflicts across teams but cannot do anything about conflicts generated within a
team. Smaller teams leads to fewer conflicts.

Argus uses one or more (generally one) jar files per layer to provide independent developability.

### Booch Principle

We rarely see code that reads like well written prose. In structure alone, we have been indoctrinated for decades to
write code that has only marginally improved over the lists of binary numbers that characterized Alan Turing's initial
forays into writing programs. We graduated to assembly code, which were lists of statements. No prose yet.

Then we started using "high-order" languages, like Cobol, C, Fortran, Algol.  Easier to read lists of statements. But
still nothing that you see in well written prose. No sentences, paragraphs, chapters, books, etc.

It was only with Donald Knuth's [Literate Programming](https://en.wikipedia.org/wiki/Literate_programming)
that prose like code started to appear.  But Literate Programming landed with a thud, is rarely seen and is difficult
to construct.

Clean Code, leveraging functional programming style, does allow for more prose like code. Lines of code are generally
longer and more sentence like. Functions are paragraph like. Files become chapter like. Module or components can be
considered as collections of chapters. And the overall code base tells a story. Grady Booch sees us software developers
as first and foremost, story tellers. This then, is the story of Argus.

#### Examples
```kotlin
    override suspend fun fetchLines(uri: URI): List<Profile> =
        uri.toURL().readText().split("\n").filter { it.isNotEmpty() }.map { it.loadLine() }

    private fun String.loadLine(): Profile = Json.decodeFromString(TestProfileSerializer, this)

    override suspend fun persist(item: Profile?, uri: URI) { if (item == null) updateAll(uri) else item.update(uri) }
```

## Building Pajato Projects

Gradle is used to build all Pajato projects. In order to do this safely, projects do not supply the Gradle wrapper
executables. See the file Gradle.md for details on why and how to get safe versions of the Gradle wrapper executables.

## Project Versions

The file Versions.md contains a (mostly) up-to-date list of current version information for each TKS project.
