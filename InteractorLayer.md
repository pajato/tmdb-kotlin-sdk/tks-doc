# argus-interactor: Use Case Architectural Layer

## Description

This *-interactor project contains artifacts supporting use cases, aka interactors. It exists to separate use case concerns and support independent developability and deployment.

## License
See the peer document LICENSE

## Project status
Started

## Documentation
The single responsibility for this project is to provide the implementation of Use Cases. In the Clean Architecture onion diagram, this project corresponds to the Use Case layer.

The Tmdb Kotlin SDK Use Cases are tbd:
