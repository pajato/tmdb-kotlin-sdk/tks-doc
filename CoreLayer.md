# CoreLayer

## Description

The innermost architectural layer, corresponds to Uncle Bob's Entity Layer.

## License

GPL, V3, See the peer document LICENSE for details.

## Project status

Started, Current version is 0.9.1

## Documentation

Examples of the main core interfaces are tbd.
