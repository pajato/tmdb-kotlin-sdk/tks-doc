# TKS Project Versions

The following table represents the current version pushed to Gitlab.

| Project             | Version | TKS Dependents (layers): [features]                                              |
|---------------------|---------|----------------------------------------------------------------------------------|
| tks-doc             | 0.9.2   |                                                                                  |
| tks-core-common     | 0.9.4   | (core, adapter): [episode, movies, network, search, season, tv], adapter: common |
| tks-core-episode    | 0.9.4   | core: search, (core, adapter): [season, tv], adapter: [common, episode]          |
| tks-core-network    | 0.9.2   | core: search, (core, adapter): tv, adapter: [common, episode, network]           |
| tks-core-search     | 0.9.5   | core: common                                                                     |
| tks-core-season     | 0.9.3   | core: [search, tv], adapter: [common, episode, season, tv]                       |
| tks-core-tv         | 0.9.3   | adapter: [common, tv]                                                            |
| tks-adapter-common  | 0.9.3   | adapter: [episode, season, tv]                                                   |
| tks-adapter-episode | 0.9.3   | adapter: [season, tv]                                                            |
| tks-adapter-network | 0.9.2   | adapter: tv                                                                      |
| tks-adapter-search  | 0.9.2   |                                                                                  |
| tks-adapter-season  | 0.9.4   | adapter: tv                                                                      |
| tks-adapter-tv      | 0.9.2   |                                                                                  |
| tks-uc-episode      | 0.9.1   |                                                                                  |
| tks-uc-search       | 0.9.1   |                                                                                  |
| tks-uc-season       | 0.9.1   |                                                                                  |
| tks-uc-tv           | 0.9.1   |                                                                                  |
