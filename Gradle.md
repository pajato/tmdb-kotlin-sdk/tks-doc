# Using Gradle Safely

Using Gradle inevitably leads to using the Gradle wrapper.  Mark Murphy, in his
[blog post](https://commonsware.com/blog/2016/09/19/reminder-check-projects-before-importing.html)
discusses why there is a serious security risk in using either of the Gradle wrapper scripts or even the Gradle wrapper
jar file.

In order to mitigate this risk, all Pajato projects do not include the unsafe Gradle wrapper files by default. Instead,
we recommend that contributors safely install a known version of Gradle, inspect the currently used Gradle version
in the file ```gradle/wrapper/gradle-properties``` (in the ```distributionUrl``` property) and load that distribution
using the command ```gradle wrapper gradle-version ...``` where ... is the gradle version used in a given project,
for example, 7.3.3
